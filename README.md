# temperature-store

This repo is about example project written by [Dropwizard](https://www.dropwizard.io/) with features:

1. [Test Driven Development](https://en.wikipedia.org/wiki/Test-driven_development)
    - [JUnit](https://junit.org/junit5/) (unit tests)
        > Testing core
    - [Cucumber](https://cucumber.io) (acceptance tests)
        > Testing REST interfaces aginst business requirements
1. [JDBI](https://jdbi.org) database access with [SQL Objects](https://jdbi.org/#_sql_objects)
    > Can use [POJOs](https://en.wikipedia.org/wiki/Plain_old_Java_object) with `SQL` without any boilerplate code.
1. [Flyway](https://flywaydb.org) migrations
    > Migrations run on every start of application. 
    >
    > Rollbacks are not easy with this config. If need rollbacks, you need commercial lisence of Flyway & implementation sofisticated way to migrate with [Dropwizard Commands](https://www.dropwizard.io/en/latest/manual/core.html#commands)
1. [Postgres](https://www.postgresql.org) database for production
1. [H2](http://www.h2database.com/) database for testing
    > Enables testing of whole application by `mvn clean test`


---
## How to start the temperature-store application

1. Start `Postgres` container
    > ```docker run --rm --name temperature-db -e POSTGRES_PASSWORD=mysecretpassword -d -p 5432:5432 postgres:13```
1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/temperature-store-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080/api/v1/temperatures`

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`

---
## Development

### TDD

> Test behavior not implementation. 

[Uncle Bobs demo about TDD with JUnit](https://youtu.be/58jGpV2Cg50?t=2631)

#### Rules
1. Write production code only to pass a failing unit test.
1. Write no more of a unit test than sufficient to fail (compilation failures are failures).
1. Write no more production code than necessary to pass the one failing unit test.

### In practice

#### Run tests

1. Command line: ```mvn clean test```
1. Eclipse IDE: `Run As | JUnit Test` on the test file what you wish to test.

#### Debug tests
1. Create breakpoint 
1. Debug file as `JUnit Test`

    > ![debug as](wiki/pictures/debug-as.png)

1. Ide will break on the breakpoint. 

    > ![breakpoint](wiki/pictures/breakpoint.png)

This works for:

1. JUnit test
1. Cucumber test

Cucumber test launher file is in the project: `src/test/java/com/moilanen/cucumber/AcceptanceTest.java`

### Eclipse
To debug application in Eclipse (which is rare because of TDD) you need  to set `Debug Configuration`

![Debug Configuration](wiki/pictures/rightClickApplicationJava.png)

And value there `server config.yml`

![debug parameter](wiki/pictures/debug-arguments.eclipse.png)

---
## Database development

Production database (Postgres) use trigger `ON UPDATE` to update `version` and `update_timestamp`. Test database (H2) do not support it on SQL level. There is production side folder `src/main/resources/db/postgres` and test side folder `src/test/resources/db/h2` to implement those things.

> **NOTE!** You have to test all database changes against _**both databases**_.

### Test settings 

`src/test/java/com/moilanen/test_settings/TestSettings.java` contains 
1. Database selection `com.moilanen.test_settings.TestSettings.getTestDatabaseType()` 
1. Start H2 gui on start `com.moilanen.test_settings.TestSettings.startGUI`

> Never commit those changes to GIT!
>
> It is usefull feature when implement new structure to database. It should work with both options. 

---
## Testing

```bash
mvn clean test

# open Cucumber report
open target/cucumber-html-reports/overview-features.html
```
