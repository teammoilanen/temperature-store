@fast @metadata
Feature: Metadata REST
  Consumer of microservice need some
  infomation about REST to implement
  client.

  Scenario: Version
  For compatibility reasons user must 
  see which version of API is used.
    When GET "api/Metadata/ApiVersion" 
    Then HTTP response status is 200
    And HTTP response type is "text/plain"
    And HTTP response value is "v1"
