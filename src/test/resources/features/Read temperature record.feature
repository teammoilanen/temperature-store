@fast @read
Feature: Read temperature records
  User wish to read values

  Scenario: Read name & value of temperature record
  	Given user create temp-store by name "read-1" and value of -273.55
  	When user queries temperature of "read-1"
    And temperature`s name is "read-1" 
    And temperature`s value is -273.55
    
  Scenario: Read status code
  	Given user create temp-store by name "read-1" and value of -273.55
  	When user queries temperature of "read-1"
    Then HTTP response status is 200

    
  @database_id
  Scenario: Read should not return id
  	Id is databases internal feature and it should not be visible for client.
  	Given user create temp-store by name "read-2" and value of 33.9
  	When user queries temperature of "read-2"
    Then temperature`s id not returned

  @404
  Scenario: Read not existing temperature
  	When user queries temperature of "read-not-exists"
    Then HTTP response status is 404  

  Scenario: Read all temperatures
  	Given user has created 2 temperature-name pairs
  	When user queries all temperatures
  	Then HTTP response status is 200
  	And created temperatures are on the list
  	
