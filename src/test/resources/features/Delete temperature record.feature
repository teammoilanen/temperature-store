@delete
Feature: Deletion of Temperature Records

  Scenario: Deletion is not implemented
  	Given user create temp-store by name "delete-1" and value of -273.55
  	When user deletes temp record "delete-1"
    Then HTTP response status is 501
