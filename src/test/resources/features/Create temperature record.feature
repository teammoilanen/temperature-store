@fast @create
Feature: Create Temperature Records
  
  Scenario: HTTP return code on create
  	Given user create temp-store by name "create-1" and value of -273.55
    Then HTTP response status is 201
    
  Scenario: Created temperature is readable over REST
  	Given user create temp-store by name "create-2" and value of -273.55
  	When user queries temperature of "create-2"
    Then HTTP response status is 200  	
    And temperature`s name is "create-2" 
    And temperature`s value is -273.55

  Scenario: Create with existing name
  	Given user create temp-store by name "create-3" and value of -273.55
  	And user create temp-store by name "create-3" and value of -273.55
    Then HTTP response status is 409
  
  Scenario: Create without name
  	Given user create temp-store by name "" and value of -273.55
    Then HTTP response status is 422
    
  Scenario: Create with name " "
  	Empty strings are not acceptaple names for temperature.
  	Given user create temp-store by name " " and value of -273.55
    Then HTTP response status is 422
  
  
  Scenario: After crate version is 1
  	Given user create temp-store by name "create-version" and value of -273.55
  	When user queries temperature of "create-version"
    Then temperature`s version is 1
  
  Scenario: After crate created timestamp
  	Given user create temp-store by name "create-create-timestamp" and value of -273.55
  	When user queries temperature of "create-create-timestamp"
    Then create timestamp is within 1 seconds of now
   
  Scenario: After crate updated timestamp 
  	Given user create temp-store by name "create-update-timestamp" and value of -273.55
  	When user queries temperature of "create-update-timestamp"
    Then update timestamp is null
  
  