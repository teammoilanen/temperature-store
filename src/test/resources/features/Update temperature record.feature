@fast @update
Feature: Update temperature
  

  Scenario: Before creation update is not available
  	When user updates temperature "update-404" to value 0.0 
    Then HTTP response status is 404
  	
  Scenario: Succesfull update returns 204
  	Given user create temp-store by name "update-204" and value of -273.55
  	When user updates temperature "update-204" to value 0.0
  	Then HTTP response status is 204
  
  Scenario: Updated value is readable from REST
  	Given user create temp-store by name "update-readable" and value of -273.55
  	When user updates temperature "update-readable" to value 0.0
  	And user queries temperature of "update-readable"
  	Then temperature`s value is 0.0
  	
  
  Scenario: Name cant be changed
  	Given user create temp-store by name "update-name" and value of -273.55
  	When user updates temperature "update-name" name to "update-name-error"
    Then HTTP response status is 400
  
  
  Scenario: On update version increases
  	Given user create temp-store by name "update-version" and value of -273.55
  	When user updates temperature "update-version" to value 0.0
  	And user queries temperature of "update-version"
    Then temperature`s version is 2
  
  Scenario: After update updated timestamp available
  	Given user create temp-store by name "update-updated-timestamp" and value of -273.55
  	When user updates temperature "update-updated-timestamp" to value 0.0
  	And user queries temperature of "update-updated-timestamp"
    Then update timestamp is within 1 second of now
  	 
  Scenario: On update crated timestamp not change
  	Given user create temp-store by name "update-created-timestamp" and value of -273.55
  	When user updates temperature "update-created-timestamp" to value 0.0
  	And user queries temperature of "update-created-timestamp"
    Then create timestamp is within 1 seconds of now

