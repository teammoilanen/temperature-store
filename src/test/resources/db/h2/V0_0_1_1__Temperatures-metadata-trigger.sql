CREATE TRIGGER update_Temperatures_meta 
BEFORE UPDATE ON Temperature.Temperatures 
FOR EACH ROW CALL "com.moilanen.db.h2.MetadataUpdateTrigger"