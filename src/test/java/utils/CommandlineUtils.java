package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommandlineUtils {

	public static String getProcessOutput(String...commandAndArguments)
			throws IOException, InterruptedException {
		ProcessBuilder processBuilder = new ProcessBuilder(commandAndArguments);

		processBuilder.redirectErrorStream(true);

		Process process = processBuilder.start();
		StringBuilder processOutput = new StringBuilder();

		try (BufferedReader processOutputReader = new BufferedReader(
				new InputStreamReader(process.getInputStream()));) {
			String readLine;

			while ((readLine = processOutputReader.readLine()) != null) {
				processOutput.append(readLine + System.lineSeparator());
			}

			process.waitFor();
		}

		return processOutput.toString().trim();
	}
}
