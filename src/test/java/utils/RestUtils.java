package utils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation.Builder;

import org.glassfish.jersey.client.JerseyClientBuilder;

import com.moilanen.cucumber.AcceptanceTest;

public class RestUtils {

	public static final Client client = new JerseyClientBuilder().build();

	public static Builder request(String path) {
		return client.target(
				String.format("http://localhost:%d/" + path,
						AcceptanceTest.SUPPORT.getLocalPort()))
				.request();
	}

}
