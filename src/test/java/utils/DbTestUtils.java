package utils;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.h2.tools.Server;
import org.jdbi.v3.core.Jdbi;

import com.moilanen.test_settings.TestDatabaseType;
import com.moilanen.test_settings.TestSettings;
import com.moilanen.utils.DbUtils;

public class DbTestUtils {

	private static boolean migrated = false;

	private static String username = "postgres";
	private static String password = "mysecretpassword";

	private synchronized static void migrateWithFlyway() {

		if (migrated) {
			return;
		}
		migrated = true;

		String[] locations = new String[2];
		locations[0] = "db/migration";

		if (TestSettings.getTestDatabaseType() == TestDatabaseType.POSTGRES) {
			locations[1] = "db/postgres";
		} else {
			locations[1] = "db/h2";
		}

		String[] schemas = {"temperature"};
		
		FluentConfiguration conf = Flyway.configure()
				.dataSource(TestSettings.getJDBC_URL(), username, password)
				.locations(locations)
				.schemas(schemas);
		if (TestSettings.getTestDatabaseType() == TestDatabaseType.POSTGRES) {
			conf.initSql("DROP SCHEMA IF EXISTS temperature CASCADE");
		}
		conf.load()
				.migrate();
	}

	private static void initJdbi() {
		Jdbi jdbi = Jdbi.create(TestSettings.getJDBC_URL(), username, password);
		DbUtils.initWithJdbi(jdbi);
	}

	public static void initDb() {
		startH2GuiIfNeeded();
		initJdbi();
		migrateWithFlyway();
	}

	private static boolean H2_GUI_start_attempted = false;

	public synchronized static void startH2GuiIfNeeded() {
		if (H2_GUI_start_attempted) {
			return;
		}
		H2_GUI_start_attempted = true;
		if (TestSettings.startH2Gui()) {
			DbTestUtils.startH2ServerGui();
		}
	}

	private static void startH2ServerGui() {
		final Server server = new Server();
		try {
			server.runTool("-tcp");
			server.runTool("-tcpAllowOthers");
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
}
