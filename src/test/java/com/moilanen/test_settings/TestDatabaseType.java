package com.moilanen.test_settings;


public enum TestDatabaseType {
	   
	H2("config.acceptance-test-h2.yml", "jdbc:h2:mem:temperature;DB_CLOSE_DELAY=-1;MODE=PostgreSQL;DATABASE_TO_LOWER=TRUE;"), 
	POSTGRES("config.acceptance-test-postgres.yml", "jdbc:postgresql://localhost:5432/?ssl=false");

	private String configFilePath;
	private String jdbcUrl;

	TestDatabaseType(String configFilePath, String jdbcUrl) {
		this.configFilePath = configFilePath;
		this.jdbcUrl = jdbcUrl;
	}
	
	public String getResourceFilePath() {
		return configFilePath;
	}

	public String getJdbcUrl() {
		return jdbcUrl;
	}
}
