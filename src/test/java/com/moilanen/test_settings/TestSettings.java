package com.moilanen.test_settings;

public class TestSettings {

	public static TestDatabaseType getTestDatabaseType() {
		return TestDatabaseType.H2;
	}
	
	private static boolean startGUI = false;

	public static boolean startH2Gui() {
		if (getTestDatabaseType() == TestDatabaseType.POSTGRES) {
			return false;
		}
		return startGUI;
	}

	private static TestDatabaseType TEST_DB = getTestDatabaseType();

	public static String getJDBC_URL() {
		return TEST_DB.getJdbcUrl();
	}

	public static String getResourceFilePath() {
		return TEST_DB.getResourceFilePath();
	}

}
