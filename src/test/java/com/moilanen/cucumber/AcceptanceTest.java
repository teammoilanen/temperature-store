package com.moilanen.cucumber;

import io.cucumber.junit.CucumberOptions;
import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.DropwizardTestSupport;
import io.dropwizard.testing.ResourceHelpers;
import utils.DbTestUtils;
import io.cucumber.junit.Cucumber;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import com.moilanen.TemperatureStoreConfiguration;
import com.moilanen.test_settings.TestSettings;
import com.moilanen.TemperatureStoreApplication;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", plugin = {
		"pretty",
		"html:target/cucumber-html-report",
		"json:target/cucumber.json",
		"junit:target/cucumber.xml",
		"rerun:target/rerun.txt"
}, tags = "not @ignore and not @integration")
public class AcceptanceTest {

	
	public static final DropwizardTestSupport<TemperatureStoreConfiguration> SUPPORT = new DropwizardTestSupport<TemperatureStoreConfiguration>(
			TemperatureStoreApplication.class,
			ResourceHelpers.resourceFilePath(TestSettings.getResourceFilePath()),
			ConfigOverride
					// Optional, if not using a separate testing-specific
					// configuration file, use a randomly selected port
					.config("server.applicationConnectors[0].port", "0"));

	@BeforeClass
	public static void before() throws Exception {
		DbTestUtils.startH2GuiIfNeeded();
		SUPPORT.before();
	}

	@AfterClass
	public static void generateReports() {
		SUPPORT.after();
		StandAloneHtmlReporter.generateReports();
	}

	

}
