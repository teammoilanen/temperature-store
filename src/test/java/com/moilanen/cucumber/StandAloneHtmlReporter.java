package com.moilanen.cucumber;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import utils.CommandlineUtils;

public class StandAloneHtmlReporter {

	private static final String ANSI_RESET = "\u001B[0m";
	private static final String ANSI_BLUE = "\u001B[34m";
	private static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
	
	static void generateReports() {
		File reportOutputDirectory = new File("target");
		List<String> jsonFiles = new ArrayList<String>();
		jsonFiles.add("target/cucumber.json");

		String buildNumber = "-1";
		String projectName = "TDD_Microservice acceptance test";

		Configuration configuration = new Configuration(
				reportOutputDirectory, projectName);
		configuration.setBuildNumber(buildNumber);
		configuration
				.addClassifications("Platform", System.getProperty("os.name"));

		try {
			configuration.addClassifications("Branch", CommandlineUtils.getProcessOutput("git", "branch", "--show-current"));
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			throw new Error(e);
		}

		ReportBuilder reportBuilder = new ReportBuilder(
				jsonFiles, configuration);
		reportBuilder.generateReports();
		// and here validate 'result' to decide what to do if report has failed
		System.out.println("Cucumber HTML reports generated.");
		System.out.println(
				ANSI_BLUE + ANSI_YELLOW_BACKGROUND
						+ "[HINT] Run in console to open report:" + ANSI_RESET);
		System.out.println(
				"open target/cucumber-html-reports/overview-features.html");
		System.out.println();
	}

}