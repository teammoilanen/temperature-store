package com.moilanen.cucumber.steps;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.moilanen.cucumber.world.RestWorld;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import utils.RestUtils;

public class RestSteps {

	private RestWorld world;

	public RestSteps(RestWorld world) {
		this.world = world;
	}
	@Given("GET {string}")
	public void get(String path) {
		Response response = RestUtils.request(path).get();
		this.world.setResponse(response);
	}

	@Then("HTTP response status is {int}")
	public void http_response_is(int status) {
		assertNotNull(this.world.getResponse());
		assertEquals(status, this.world.getResponse().getStatus());
	}

	@Then("HTTP response type is {string}")
	public void http_response_type_is(String type) {
		Response r = this.world.getResponse();
		assertNotNull(r);
		assertTrue(r.hasEntity());
		MultivaluedMap<String, Object> h = r.getHeaders();
		String contentType = (String) h.get("Content-Type").get(0);
		assertEquals(type, contentType);
	}

	@Then("HTTP response value is {string}")
	public void responses_body_is(String body) {
		assertNotNull(this.world.getResponse());
		Response response = this.world.getResponse();
		assertNotNull(response);
		String output = response.readEntity(String.class);
		assertEquals(body, output);
	}
}
