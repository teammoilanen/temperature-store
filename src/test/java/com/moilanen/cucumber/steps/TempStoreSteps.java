package com.moilanen.cucumber.steps;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Date;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.moilanen.api.TemperatureRequest;
import com.moilanen.core.temperature.TemperatureRecord;
import com.moilanen.cucumber.world.RestWorld;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utils.RestUtils;

public class TempStoreSteps {

	private static String path = "api/v1/temperatures/";

	private RestWorld world;

	public TempStoreSteps(RestWorld world) {
		this.world = world;
	}

	@Given("user create temp-store by name {string} and value of {double}")
	public void user_create_temp_store_by_name_and_value_of(String name,
			Double temperature) {

		TemperatureRequest r = new TemperatureRequest(name, temperature);
		Entity<TemperatureRequest> e = Entity.entity(r,
				MediaType.APPLICATION_JSON);
		Response response = RestUtils.request(path).post(e);
		this.world.setResponse(response);
	}

	@When("user deletes temp record {string}")
	public void user_deletes_temp_record(String name) {
		Response response = RestUtils.request(path + name).delete();
		this.world.setResponse(response);
	}

	@Given("user has created {int} temperature-name pairs")
	public void user_has_created_temperature_name_pairs(Integer number) {
		this.user_create_temp_store_by_name_and_value_of("batch-1", -1d);
		this.user_create_temp_store_by_name_and_value_of("batch-2", -2d);
	}

	@When("user queries temperature of {string}")
	public void ask_temperature_of(String name) {
		Response response = RestUtils.request(path + name).get();
		this.world.setResponse(response);
		try {
			TemperatureRecord entity = response
					.readEntity(TemperatureRecord.class);
			this.world.setEntity(entity);
		} catch (Throwable e) {
			// this is not test "then" step, -> ignore
		}
	}

	@When("user queries all temperatures")
	public void user_queries_all_temperatures() {
		Response response = RestUtils.request(path).get();
		this.world.setResponse(response);
		List<TemperatureRecord> entities = response
				.readEntity(new GenericType<List<TemperatureRecord>>() {
				});
		this.world.setEntity(entities);
	}

	@Then("created temperatures are on the list")
	public void created_temperatures_are_on_the_list() {
		List<TemperatureRecord> entities = this.world.getEntity();

		assertTrue(entities.stream().anyMatch(t -> {
			return t.getName().equals("batch-1") && t.getTemperature() == -1d;
		}));

		assertTrue(entities.stream().anyMatch(t -> {
			return t.getName().equals("batch-2") && t.getTemperature() == -2d;
		}));

	}

	@Then("temperature`s name is {string}")
	public void temperature_s_name_is(String name) {
		TemperatureRecord entity = this.world.getEntity();
		assertNotNull(entity);
		assertEquals(name, entity.getName());
	}

	@Then("temperature`s value is {double}")
	public void temperature_s_value_is(Double temp) {
		TemperatureRecord entity = this.world.getEntity();
		assertNotNull(entity);
		assertEquals(temp, entity.getTemperature());
	}

	@Then("temperature`s id not returned")
	public void temperature_s_id_not_returned() {
		TemperatureRecord entity = this.world.getEntity();
		assertNotNull(entity);
		assertNull(entity.getId());
	}

	@Then("temperature`s version is {int}")
	public void temperature_s_version_is(Integer version) {
		TemperatureRecord entity = this.world.getEntity();
		assertNotNull(entity);
		assertEquals(version, entity.getVersion());
	}

	@Then("create timestamp is within {int} seconds of now")
	public void create_timestamp_is_within_seconds_of_now(Integer seconds) {
		TemperatureRecord entity = this.world.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getCreated());
		long difference = Math
				.abs(entity.getCreated().getTime() - new Date().getTime());
		assertTrue(difference < seconds * 1000);
	}

	@Then("update timestamp is within {int} second of now")
	public void update_timestamp_is_within_second_of_now(Integer seconds) {
		TemperatureRecord entity = this.world.getEntity();
		assertNotNull(entity);
		assertNotNull(entity.getCreated());
		long difference = Math
				.abs(entity.getUpdated().getTime() - new Date().getTime());
		assertTrue(difference < seconds * 1000);
	}

	@Then("update timestamp is null")
	public void update_timestamp_is_null() {
		TemperatureRecord entity = this.world.getEntity();
		assertNotNull(entity);
		assertNull(entity.getUpdated());
	}

	@When("user updates temperature {string} to value {double}")
	public void user_updates_temperature_to_value(String name,
			Double temperature) {
		TemperatureRequest r = new TemperatureRequest(name, temperature);
		Entity<TemperatureRequest> e = Entity.entity(r,
				MediaType.APPLICATION_JSON);
		Response response = RestUtils.request(path + name).put(e);
		this.world.setResponse(response);
	}

	@When("user updates temperature {string} name to {string}")
	public void user_updates_temperature_name_to(String name,
			String changedName) {
		TemperatureRequest r = new TemperatureRequest(changedName, null);
		Entity<TemperatureRequest> e = Entity.entity(r,
				MediaType.APPLICATION_JSON);
		Response response = RestUtils.request(path + name).put(e);
		this.world.setResponse(response);
	}
}
