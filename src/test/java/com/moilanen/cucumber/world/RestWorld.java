package com.moilanen.cucumber.world;

import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.JsonNode;

public class RestWorld {

	private Response response;
	private JsonNode json;
	private String jwt;
	private Object entity;

	public void setResponse(Response response) {
		this.response = response;
	}

	public Response getResponse() {
		return this.response;
	}

	public JsonNode getJson() {
		return json;
	}

	public void setJson(JsonNode json) {
		this.json = json;
	}

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	public void setEntity(Object entity) {
		this.entity = entity;
	}
	
	@SuppressWarnings("unchecked")
	public <E> E getEntity() {
		return (E) this.entity;
	}

	

}
