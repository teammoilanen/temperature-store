package com.moilanen.db.h2;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.h2.api.Trigger;

public class MetadataUpdateTrigger implements Trigger {

	@Override
	public void init(Connection conn, String schemaName,
			String triggerName, String tableName, boolean before, int type)
			throws SQLException {
	}

	@Override
	public void fire(Connection conn, Object[] oldRow, Object[] newRow)
			throws SQLException {

		updateVersion(newRow, 1);
		updateUpdateTimestamp(newRow, 3);
	}

	private void updateUpdateTimestamp(Object[] newRow, int i) {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		newRow[i] = now;
	}

	private void updateVersion(Object[] newRow, int i) {
		Integer version = (int) newRow[i];
		newRow[i] = version + 1;
	}

	@Override
	public void close() throws SQLException {
	}

	@Override
	public void remove() throws SQLException {
	}
}
