package com.moilanen.db.temperature;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import javax.ws.rs.WebApplicationException;

import org.jdbi.v3.core.statement.UnableToExecuteStatementException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.moilanen.core.temperature.TemperatureRecord;
import com.moilanen.db.IDao;
import utils.DbTestUtils;

public class TemperatureRecordTest {

	private static IDao<TemperatureRecord> dao;

	@BeforeAll
	static void initDb() {
		DbTestUtils.initDb();
		dao = new TemperatureRecordDAO();
	}

	private TemperatureRecord tempRecord;
	private int i;

	@BeforeEach
	void createEmptyEntity() {
		tempRecord = new TemperatureRecord();
		tempRecord.setName("tempRecordTest-" + ++i);
	}

	@Test
	void persistedEntity_isReadableFromDatabase() {
		tempRecord.setName("k1");
		tempRecord.setTemperature(1.1);
		dao.persist(tempRecord);

		TemperatureRecord inDatabase = dao.getById(tempRecord.getId());
		assertEquals(tempRecord, inDatabase);
		assertEquals(tempRecord.getName(), inDatabase.getName());
		assertEquals(tempRecord.getTemperature(), inDatabase.getTemperature());
	}

	@Test
	void name_cantBeNull() {

		tempRecord.setName(null);

		assertThrows(
				NullPointerException.class,
				() -> dao.persist(tempRecord));
	}

	@Test
	void name_isUnique() {

		tempRecord.setName("unique");
		dao.persist(tempRecord);

		TemperatureRecord tempRecord2 = new TemperatureRecord();
		tempRecord2.setName("unique");
		assertThrows(
				WebApplicationException.class,
				() -> dao.persist(tempRecord2));
	}

	@Test
	void name_isNotUpdateable() {

		// should implement trigger. Now done in REST service.
		// tempRecord.setName("non-updateable");
		// dao.persist(tempRecord);
		//
		// tempRecord.setName("non-updateable-updated");
		//
		// assertThrows(
		// WebApplicationException.class,
		// () -> dao.update(tempRecord));

	}

}
