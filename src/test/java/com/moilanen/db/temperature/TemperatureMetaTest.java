package com.moilanen.db.temperature;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.Date;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.moilanen.core.temperature.TemperatureRecord;
import com.moilanen.db.IDao;
import utils.DbTestUtils;

public class TemperatureMetaTest {

	private static IDao<TemperatureRecord>  dao;

	@BeforeAll
	static void initDb() {
		DbTestUtils.initDb();
		dao = new TemperatureRecordDAO();
	}

	private TemperatureRecord tempNamePair;
	private static int i;

	@BeforeEach
	void createEmptyEntity() {
		tempNamePair = new TemperatureRecord();
		tempNamePair.setName("metatest-" + ++i);
	}
	
	@Test
	void onUpdate_versionIncreases() {
		assertEquals(null, tempNamePair.getVersion());
		dao.persist(tempNamePair);

		assertEquals(1, tempNamePair.getVersion());

		tempNamePair.setTemperature(-1.0d);
		dao.update(tempNamePair);
		assertEquals(2, tempNamePair.getVersion());
	}

	@Test
	void createTimestamp_onPersist() {
		Date before = new Date();

		dao.persist(tempNamePair);
		Date afterInsert = new Date();

		assertTrue(before.before(tempNamePair.getCreated()));
		assertFalse(tempNamePair.getCreated().after(afterInsert));
	}

	@Test
	void updateTimestamp_onPersist() {

		dao.persist(tempNamePair);
		assertNull(tempNamePair.getUpdated());
	}

	@Test
	void updateTimestamp_onUpdate() {
		Date beforeInsert = new Date();

		dao.persist(tempNamePair);

		tempNamePair.setTemperature(-100d);
		dao.update(tempNamePair);

		Date afterUpdate = new Date();
		assertNotNull(tempNamePair.getUpdated());
		assertTrue(tempNamePair.getUpdated().after(beforeInsert));
		assertFalse(tempNamePair.getUpdated().after(afterUpdate));
	}

	@Test
	void onUpdate_creteTimestampNotChange() {

		dao.persist(tempNamePair);

		Date createdTimestamp = tempNamePair.getCreated();

		tempNamePair.setTemperature(25.1);
		dao.update(tempNamePair);

		Date createdTimestamp2 = tempNamePair.getCreated();

		assertEquals(createdTimestamp, createdTimestamp2);
	}
}
