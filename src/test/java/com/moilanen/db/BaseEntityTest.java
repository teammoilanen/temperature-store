package com.moilanen.db;

import static org.junit.Assert.assertThrows;
import org.junit.jupiter.api.Test;

import com.moilanen.core.temperature.TemperatureRecord;

public class BaseEntityTest {

	@Test
	void metadata_canNotBeNull() {

		TemperatureRecord t = new TemperatureRecord();
		assertThrows(
				NullPointerException.class,
				() -> t.setMeta(null));
	}

}
