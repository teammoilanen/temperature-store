CREATE TABLE Temperature.Temperatures (
  id bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
  "version" int NOT NULL DEFAULT 1,
  created timestamp NOT NULL DEFAULT current_timestamp,
  updated timestamp,
  "name" varchar(255) NOT NULL,
  temperature DECIMAL(10,2) null,
  CONSTRAINT keyvalue_pk PRIMARY KEY (id)
);
COMMENT ON COLUMN Temperature.Temperatures.id IS 'id, version, created & updated colums have to be 1st ones in the table and in that order for H2 testing trigger compatibility';

CREATE UNIQUE INDEX temperature_name ON Temperature.Temperatures ("name");