CREATE TRIGGER update_Temperatures_meta 
BEFORE UPDATE ON Temperature.Temperatures 
FOR EACH ROW EXECUTE FUNCTION update_meta_columns();