CREATE OR REPLACE FUNCTION update_meta_columns()
RETURNS TRIGGER AS $$
BEGIN
   NEW.updated = now(); 
   NEW.version = NEW.version + 1;
   RETURN NEW;
END;
$$ language 'plpgsql';
