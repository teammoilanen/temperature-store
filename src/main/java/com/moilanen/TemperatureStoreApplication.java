package com.moilanen;

import org.flywaydb.core.Flyway;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moilanen.utils.DbUtils;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.db.ManagedDataSource;
import io.dropwizard.flyway.FlywayBundle;
import io.dropwizard.flyway.FlywayFactory;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class TemperatureStoreApplication
		extends
			Application<TemperatureStoreConfiguration> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(TemperatureStoreApplication.class);

	public static void main(final String[] args) throws Exception {
		new TemperatureStoreApplication().run(args);
	}

	@Override
	public String getName() {
		return "temperature-store";
	}

	@Override
	public void initialize(Bootstrap<TemperatureStoreConfiguration> bootstrap) {
		bootstrap.addBundle(new FlywayBundle<TemperatureStoreConfiguration>() {
			@Override
			public DataSourceFactory getDataSourceFactory(
					TemperatureStoreConfiguration configuration) {
				return configuration.getDataSourceFactory();
			}

			@Override
			public FlywayFactory getFlywayFactory(
					TemperatureStoreConfiguration configuration) {
				return configuration.getFlywayFactory();
			}
		});
	}

	@Override
	public void run(final TemperatureStoreConfiguration configuration,
			final Environment environment) {

		this.flywayMigration(configuration, environment);

		final Jdbi jdbi = initJdbi(configuration, environment);
		DbUtils.initWithJdbi(jdbi);

		ResourceRegisterer.registerAll(environment);
	}

	private Jdbi initJdbi(TemperatureStoreConfiguration configuration,
			Environment environment) {

		final JdbiFactory factory = new JdbiFactory();
		final Jdbi jdbi = factory.build(environment,
				configuration.getDataSourceFactory(), "postgres");
		return jdbi;
	}

	private void flywayMigration(TemperatureStoreConfiguration configuration,
			Environment environment) {

		try {
			LOGGER.info("begin migration");
			DataSourceFactory dsFactory = configuration.getDataSourceFactory();
			ManagedDataSource ds = dsFactory.build(environment.metrics(),
					"flyway");
			FlywayFactory fwFactory = configuration.getFlywayFactory();
			Flyway flyway = fwFactory.build(ds);
			flyway.migrate();
			LOGGER.info("finish migration");
		} catch (Exception e) {
			LOGGER.error("migration failed");
			e.printStackTrace();
			throw new Error(e);
		}
	}

}
