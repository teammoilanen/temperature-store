package com.moilanen;

import com.moilanen.filter.CORSFilter;
import com.moilanen.resources.MetadataResource;
import com.moilanen.resources.TemperatureResource;

import io.dropwizard.setup.Environment;

public class ResourceRegisterer {

	public static void registerAll(Environment environment) {

		environment.jersey().register(new CORSFilter());
		environment.jersey().register(new MetadataResource());
		environment.jersey().register(new TemperatureResource());
	}

}
