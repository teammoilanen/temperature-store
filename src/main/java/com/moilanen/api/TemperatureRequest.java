package com.moilanen.api;

import javax.validation.constraints.NotBlank;

public class TemperatureRequest {

	@NotBlank
	private String name;
	private Double temperature;

	public TemperatureRequest() {
	}

	public TemperatureRequest(String name, Double temperature) {
		this.setName(name);
		this.setTemperature(temperature);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getTemperature() {
		return temperature;
	}

	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}

}
