package com.moilanen.core.temperature;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.moilanen.api.TemperatureRequest;
import com.moilanen.db.temperature.TemperatureRecordDAO;

public class TemperatureRecordService {

	static TemperatureRecordDAO dao = new TemperatureRecordDAO();

	public static TemperatureRecord create(
			@Valid @NotNull TemperatureRequest api) {
		TemperatureRecord toPersist = TemperatureRecordFactory
				.fromApi(api);
		dao.persist(toPersist);
		return toPersist;
	}

	public static void update(
			String name, @Valid @NotNull TemperatureRequest api) {

		TemperatureRecord inDatabase = dao.getByName(name);
		if (inDatabase == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		TemperatureRecord toUpdate = TemperatureRecordFactory
				.fromApi(api);

		if (!toUpdate.getName().equals(name)) {
			throw new WebApplicationException("name is not changeable",
					Response.Status.BAD_REQUEST);
		}

		toUpdate.setId(inDatabase.getId());

		dao.update(toUpdate);

	}

}
