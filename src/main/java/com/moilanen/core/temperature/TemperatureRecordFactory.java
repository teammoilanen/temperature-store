package com.moilanen.core.temperature;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.moilanen.api.TemperatureRequest;

public class TemperatureRecordFactory {

	public static TemperatureRecord fromApi(
			@Valid @NotNull TemperatureRequest api) {
		
		TemperatureRecord ret = new TemperatureRecord();
		ret.setName(api.getName());
		ret.setTemperature(api.getTemperature());
		return ret;
	}

}
