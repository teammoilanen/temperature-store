package com.moilanen.core.temperature;

import com.moilanen.core.BaseEntity;

public class TemperatureRecord extends BaseEntity {

	private String name;
	private Double temperature;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getTemperature() {
		return temperature;
	}
	public void setTemperature(Double temperature) {
		this.temperature= temperature;
	}
}
