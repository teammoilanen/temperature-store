package com.moilanen.core;

import java.util.Date;
import java.util.Objects;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public abstract class BaseEntity {

	public enum State {
		TRANSIENT, PERSISTENT
	}

	private Long id;
	private Integer version;
	private Date created;
	private Date updated;

	public void setMeta(EntityMetadata meta) {
		Objects.requireNonNull(meta);
		this.setId(meta.getId());
		this.setCreated(meta.getCreated());
		this.setUpdated(meta.getUpdated());
		this.setVersion(meta.getVersion());
	}

	// Do not expose this to client. It is database's internal
	// value.
	@JsonIgnore
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@JsonInclude(Include.NON_NULL)
	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	@JsonInclude(Include.NON_NULL)
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@JsonInclude(Include.NON_NULL)
	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	@JsonIgnore
	public boolean isPersisted() {
		return this.getState() == State.PERSISTENT;
	}

	@Override
	public synchronized int hashCode() {
		if (!this.isPersisted()) {
			return super.hashCode();
		}

		HashCodeBuilder b = new HashCodeBuilder(7, 11);
		b.append(this.getClass());
		b.append(this.getId());
		return b.hashCode();

	}

	@JsonIgnore
	public State getState() {

		if (this.id == null) {
			return State.TRANSIENT;
		}
		return State.PERSISTENT;
	}

	/**
	 * Transient instance equals only for same instance. Persistent relies ID
	 * &amp; class name.
	 */
	@Override
	public boolean equals(Object other) {

		if (other == null) {
			return false;
		}

		if (this == other) {
			return true;
		}

		if (this.getClass() != other.getClass()) {
			return false;
		}

		final BaseEntity e = (BaseEntity) other;

		if (this.isPersisted() != e.isPersisted()) {
			return false;
		}

		if (!this.isPersisted()) {
			if (this.hashCode() == other.hashCode()) {
				// transient instance is same
				return true;
			}
			return false;
		}

		// rely on database identity
		if (this.getId().equals(e.getId())) {
			return true;
		}

		return false;
	}

}
