package com.moilanen.utils;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

public class DbUtils {

	private static Jdbi jdbi;

	public static Jdbi getJdbi() {
		return jdbi;
	}

	public static void initWithJdbi(Jdbi jdbi2) {
		jdbi = jdbi2;
		jdbi.installPlugin(new SqlObjectPlugin());
		
	}

}
