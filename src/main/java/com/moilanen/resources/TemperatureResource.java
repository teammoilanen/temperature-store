package com.moilanen.resources;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.codahale.metrics.annotation.Timed;
import com.moilanen.api.TemperatureRequest;
import com.moilanen.core.temperature.TemperatureRecordService;
import com.moilanen.core.temperature.TemperatureRecord;
import com.moilanen.db.temperature.TemperatureRecordDAO;

@Path("api/v1/temperatures")
public class TemperatureResource {

	TemperatureRecordDAO dao = new TemperatureRecordDAO();

	@POST
	@Timed
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createTemperature(@Valid @NotNull TemperatureRequest api) {

		TemperatureRecord created = TemperatureRecordService.create(api);

		return Response
				.created(
						UriBuilder.fromResource(TemperatureResource.class)
								.build(created.getName()))
				.build();
	}

	@GET
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	public List<TemperatureRecord> readTemperatures() {
		return dao.getAll();
	}

	@GET
	@Path("/{name}")
	@Timed
	@Produces(MediaType.APPLICATION_JSON)
	public TemperatureRecord readTemperature(@PathParam("name") String name) {
		TemperatureRecord ret = dao.getByName(name);
		if (ret == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		return ret;
	}

	@PUT
	@Path("/{name}")
	@Timed
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateTemperature(@PathParam("name") String name,
			@Valid @NotNull TemperatureRequest api) {

		TemperatureRecordService.update(name, api);

		return Response.noContent().build();
	}

	@DELETE
	@Path("/{name}")
	@Timed
	public Response deleteTemperature(@PathParam("name") String name) {
		throw new WebApplicationException(Response.Status.NOT_IMPLEMENTED);
	}
}
