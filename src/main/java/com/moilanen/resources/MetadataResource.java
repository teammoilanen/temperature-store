package com.moilanen.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.codahale.metrics.annotation.Timed;

@Path("/api/Metadata")
public class MetadataResource {

	@Path("ApiVersion")
	@GET
	@Timed
	@Produces("text/plain")
	public String getApiVersion() {
		return "v1";
	}

}
