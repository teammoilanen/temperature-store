package com.moilanen;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.flyway.FlywayFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.*;

public class TemperatureStoreConfiguration extends Configuration {

	@Valid
	@NotNull
	private DataSourceFactory dataSourceFactory;
	private FlywayFactory flyway;

	@JsonProperty("database")
	public void setDataSourceFactory(DataSourceFactory factory) {
		this.dataSourceFactory = factory;
	}

	@JsonProperty("database")
	public DataSourceFactory getDataSourceFactory() {
		return dataSourceFactory;
	}

	@JsonProperty("flyway")
	public void setFlywayFactory(FlywayFactory factory) {
		this.flyway = factory;
	}

	@JsonProperty("flyway")
	public FlywayFactory getFlywayFactory() {
		return this.flyway;
	}
}
