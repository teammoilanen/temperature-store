package com.moilanen.db;

public interface IDao<E> {

	void persist(E entity);

	E getById(Long id);

	void update(E entity);
}
