package com.moilanen.db.temperature;

import java.util.List;

import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.customizer.BindMethods;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import com.moilanen.core.EntityMetadata;
import com.moilanen.core.temperature.TemperatureRecord;

public interface TemperatureQueries {

	@SqlUpdate("INSERT INTO temperature.temperatures (\"name\", temperature) VALUES(:getName, :getTemperature)")
	@GetGeneratedKeys({"id", "version", "created", "updated"})
	@RegisterBeanMapper(EntityMetadata.class)
	EntityMetadata persist(@BindMethods TemperatureRecord entity);

	@SqlQuery("select * from temperature.temperatures where id = ?")
	@RegisterBeanMapper(TemperatureRecord.class)
	TemperatureRecord getById(Long id);
	
	@SqlQuery("select * from temperature.temperatures where \"name\" = ?")
	@RegisterBeanMapper(TemperatureRecord.class)
	TemperatureRecord getByName(String name);

	@SqlQuery("select * from temperature.temperatures")
	@RegisterBeanMapper(TemperatureRecord.class)
	List<TemperatureRecord> getAll();

	@SqlUpdate("UPDATE temperature.temperatures SET temperature=:temperature WHERE id=:id")
	@GetGeneratedKeys({"id", "version", "created", "updated"})
	@RegisterBeanMapper(EntityMetadata.class)
	EntityMetadata update(@BindBean TemperatureRecord entity);

}
