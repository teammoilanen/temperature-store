package com.moilanen.db.temperature;

import java.util.List;
import java.util.Objects;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.jdbi.v3.core.statement.UnableToExecuteStatementException;

import com.moilanen.core.EntityMetadata;
import com.moilanen.core.temperature.TemperatureRecord;
import com.moilanen.db.IDao;
import com.moilanen.utils.DbUtils;

public class TemperatureRecordDAO implements IDao<TemperatureRecord> {

	public void persist(TemperatureRecord entity) {
		Objects.requireNonNull(entity.getName());
		try {
			EntityMetadata meta = DbUtils.getJdbi().withHandle(handle -> {

				return handle
						.attach(TemperatureQueries.class)
						.persist(entity);
			});
			entity.setMeta(meta);
		} catch (UnableToExecuteStatementException v) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}
	}

	@Override
	public void update(TemperatureRecord entity) {
		Objects.requireNonNull(entity.getId());

		EntityMetadata meta = DbUtils.getJdbi().withHandle(handle -> {
			return handle
					.attach(TemperatureQueries.class)
					.update(entity);
		});
		entity.setMeta(meta);
	}

	public TemperatureRecord getByName(String name) {
		TemperatureRecord entity = DbUtils.getJdbi().withHandle(handle -> {
			return handle
					.attach(TemperatureQueries.class)
					.getByName(name);
		});
		return entity;
	}

	@Override
	public TemperatureRecord getById(Long id) {
		TemperatureRecord entity = DbUtils.getJdbi().withHandle(handle -> {
			return handle
					.attach(TemperatureQueries.class)
					.getById(id);
		});
		return entity;
	}

	public List<TemperatureRecord> getAll() {
		return DbUtils.getJdbi().withHandle(handle -> {
			return handle
					.attach(TemperatureQueries.class)
					.getAll();
		});
	}

}
